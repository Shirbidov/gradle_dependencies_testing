package az.ingress.gradle_lesson9.gradle3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Gradle3Application {

	public static void main(String[] args) {
		SpringApplication.run(Gradle3Application.class, args);
	}

}
